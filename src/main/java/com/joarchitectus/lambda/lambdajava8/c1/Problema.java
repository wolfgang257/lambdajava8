/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.joarchitectus.lambda.lambdajava8.c1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Problema {

    public static void main(String[] args) {

        List<String> nombres = Arrays.asList("Juan", "Antonia", "Pedro");

        Comparator<String> comparadorLongitud
                = (o1, o2) -> o1.length() - o2.length();
        Collections.sort(nombres, comparadorLongitud);

        System.out.println(nombres);
    }
}
