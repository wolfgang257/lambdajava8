/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.joarchitectus.lambda.lambdajava8.c2;

import java.util.Collection;

/**
 *
 * @author josorio
 */
public interface Agrupador {

    void add(Object elemento);

    int numeroElementos();

    default void addAll(Collection col) {
        for (Object o : col) {
            add(o);
        }
    }
}
