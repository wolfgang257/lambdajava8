/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.joarchitectus.lambda.lambdajava8.c2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author josorio
 */
public class AgrupadorConList implements Agrupador {

    private List<Object> contenido = new ArrayList<>();

    @Override
    public void add(Object elemento) {
        contenido.add(elemento);
    }

    @Override
    public int numeroElementos() {
        return contenido.size();
    }

}
