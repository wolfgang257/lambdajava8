/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.joarchitectus.lambda.lambdajava8.c2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author josorio
 */
public class AgrupadorConArray implements Agrupador {

    private Object[] contenido = new Object[20];
    private int index = 0;

    @Override
    public void add(Object elemento) {
        contenido[index++] = elemento;
    }

    @Override
    public int numeroElementos() {
        return index;
    }

}
